//
//  ViewController.swift
//  RequisicaoNovaIOS
//
//  Created by COTEMIG on 22/09/22.
//

import UIKit
import Kingfisher
import Alamofire

struct Cachorro: Decodable{
    let message: String
    let status: String
}

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getNovoCachorro()
    }

    @IBAction func recarregarImagem(_ sender: Any) {
        getNovoCachorro()
    }
    
    func getNovoCachorro(){
        AF.request("https://dog.ceo/api/breeds/image/random").responseDecodable(of: Cachorro.self) { response in
            if let cachorro = response.value {
                self.imageView.kf.setImage(with: URL(string: cachorro.message))
            }
        }
    }
}

